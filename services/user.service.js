let fs = require("fs");

const getUsers = () => {
    let content = fs.readFileSync(__dirname + "/../data/userlist.json");
    return users = JSON.parse(content);
}

const getUserById = (_id) => {

    let users = getUsers();

    let selectedUser;

    if (Array.isArray(users)) {
        for(let i=0; i<users.length; i++){
            if(users[i]._id == _id) {
                selectedUser = users[i];
                break;
            }
        }
        
        if(selectedUser) {
            return selectedUser;
        } else {
            return null;
        }
    };

    return null;
}

const deleteUserById = (_id) => {
    let users = getUsers();
    let index  = -1;

    if (Array.isArray(users)) {
        for (let i = 0; i < users.length; i++) {
            if (users[i]._id == _id) {
                selectedUser = users[i];
                index = i;
                break;
            }
        }

        if (index > -1) {
            let user = users.splice(index,1)[0];
            let data = JSON.stringify(users);
            fs.writeFileSync(__dirname + "/../data/userlist.json",data);
            return user;
        } else {
            return null;
        }
    };

    return null;
}

const createNewUser = (user) =>{

    const users = getUsers();

    if(Array.isArray(users)) {
        const ids = users.map((element) =>{
            return element._id;
        })

        let new_id = Math.max(...ids) + 1;

        const newUser = {
            "_id": new_id,
            "name": user.name,
            "health": user.health,
            "attack": user.attack,
            "defense": user.defense,
            "source": user.source
        };

        users.push(newUser);

        let data = JSON.stringify(users);
        fs.writeFileSync(__dirname + "/../data/userlist.json", data);

        return newUser;
    }

    return null;
}

const updateUser = (_id, user) => {
    const users = getUsers();

    const updateUserData = {
        "_id": _id,
        "name": user.name,
        "health": user.health,
        "attack": user.attack,
        "defense": user.defense,
        "source": user.source        
    };

    let updatedRecord = false;
    for(let i=0; users.length; i++) {
        if(users[i]._id == updateUserData._id){    
            users[i].name = updateUserData.name;
            users[i].health = updateUserData.health;
            users[i].attack = updateUserData.attack;
            users[i].defense = updateUserData.defense;
            users[i].source = updateUserData.source;

            updatedRecord = true;
            break;
        }
    };

    if(updatedRecord) {
        let data = JSON.stringify(users);
        fs.writeFileSync(__dirname + "/../data/userlist.json", data);        
    }

    return updateUserData;
}

module.exports = {
    getUsers,
    getUserById,
    deleteUserById,
    createNewUser,
    updateUser,
}