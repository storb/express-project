let express = require('express');
let fs = require("fs");
const bodyParser = require('body-parser');
const jsonParser = bodyParser.json();

const { getUsers, getUserById, deleteUserById, createNewUser, updateUser } = require("../services/user.service");
let router = express.Router();


/* GET users listing. */
router.get('/', function (req, res, next) {    
    res.send(getUsers());
});

/* GET user by ID */
router.get('/:id', function (req, res, next) {
    const _id = parseInt(req.params.id);

    if (Number.isInteger(_id)) {
        const user = getUserById(_id);
        if(user) {
            res.send(user);
        } else {
            res.status(404).send("NotFound");    
        }
        
    } else {
        res.status(404).send("NotFound");
    }   
});

/* Delete user by ID */
router.delete('/:id', function (req, res, next) {
    const _id = parseInt(req.params.id);

    if (Number.isInteger(_id)) {
        const user = deleteUserById(_id);
        if (user) {
            res.send(user);
        } else {
            res.status(404).send("NotFound");
        }

    } else {
        res.status(404).send("NotFound");
    }
});

/* Create New User */
router.post('/', jsonParser ,function(req, res, next){
    if(!req.body) {
        console.log(req.body);
        return res.status(400).send("NotPermitted");    
    }

    const user = {
        name: req.body.name,
        health: req.body.health,
        attack: req.body.attack,
        defense: req.body.defense,
        source: req.body.source
    }

    if(createNewUser(user)){
        res.status(200).send("Created");
    } else {

    };

});

router.put('/:id', jsonParser, function (req, res, next) {
    if (!req.body) {
        console.log(req.body);
        return res.status(400).send("NotPermitted");
    }

    const _id = parseInt(req.params.id);

    const user = {
        name: req.body.name,
        health: req.body.health,
        attack: req.body.attack,
        defense: req.body.defense,
        source: req.body.source
    }

    if (updateUser(_id,user)) {
        res.status(200).send("Updated");
    } else {

    };

});

module.exports = router;